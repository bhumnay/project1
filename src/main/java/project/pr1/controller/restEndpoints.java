package project.pr1.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class restEndpoints {
    @GetMapping("/course")
    public Course getEndpoint(@RequestParam(value="name",defaultValue="Maths",required=false) String name,
                              @RequestParam(value="chapterCount",defaultValue="2",required=false) int chapterCount) {
        return new Course(name,chapterCount);
    }
    @PostMapping("/register/course")
    public String saveCourse(@RequestBody Course course){
        return "Your course named "+course.getName()+" with "+course.getChapterCount()+" chapters saved successfully.";
    }
}
